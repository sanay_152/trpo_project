﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using ClassLib;
using System.Linq;
using System.Xml.Serialization;
using System.IO;
using System.Windows.Markup.Localizer;

namespace Project_po_mod
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<ReportClass> rListforReport = new List<ReportClass>();
        void DarkModeIsReal(bool? ixs)
        {
            var dark = new SolidColorBrush(Color.FromRgb(33, 33, 33));
            var blond = new SolidColorBrush(Color.FromRgb(222, 222, 222));
            if (ixs != true)
            {
                DarkMode.IsChecked = false;
                DarkMode_s.IsChecked = false;
                DarkMode_3td.IsChecked = false;
                DarkMode_final.IsChecked = false;
                
                    
                //MessageBox.Show("off");
                foreach (System.Windows.Controls.Grid tb in TIDARK.Children.OfType<Grid>())
                {
                    tb.Background = new SolidColorBrush(Color.FromRgb(247, 86, 181));
                    foreach (System.Windows.Controls.TextBox tb_text in tb.Children.OfType<TextBox>())
                    {
                        tb_text.Background = blond;
                        tb_text.Foreground = dark;
                    }
                }
                foreach (System.Windows.Controls.TextBox tb_text in PlatFinal.Children.OfType<TextBox>())
                {
                    tb_text.Background = blond;
                    tb_text.Foreground = dark;
                }
                sometext.Background = new SolidColorBrush(Color.FromRgb(247, 86, 181));
                sometext.Foreground = blond;
                PlatFinal.Background = new SolidColorBrush(Color.FromRgb(247, 86, 181));
                TIDARK.Background = new SolidColorBrush(Color.FromRgb(214, 230, 196));
                td3grid.Background= new SolidColorBrush(Color.FromRgb(214, 230, 196));
                Start_page.Background = new SolidColorBrush(Color.FromRgb(214, 230, 196));
                Startyem.Background = new SolidColorBrush(Color.FromRgb(214, 230, 196));
                DarkMode.Foreground = new SolidColorBrush(Color.FromRgb(255, 255, 255));
                TIfinal.Background= new SolidColorBrush(Color.FromRgb(214, 230, 196));
                DarkMode_s.Foreground = new SolidColorBrush(Color.FromRgb(255, 255, 255));
                ButtonActivacia.Background = new SolidColorBrush(Color.FromRgb(247, 86, 181));
                ButtonActivacia.Foreground = new SolidColorBrush(Color.FromRgb(222,222,222));
                Exit_ff.Background = new SolidColorBrush(Color.FromRgb(247, 86, 181));
                Exit_Finish.Background= new SolidColorBrush(Color.FromRgb(247, 86, 181));
                Exit_Finish.Foreground=blond;
                Exit_3е.Background= new SolidColorBrush(Color.FromRgb(247, 86, 181));
                Exit_3е.Foreground= blond;
                Exit_ff.Foreground = blond;
                Exit_sm.Background = new SolidColorBrush(Color.FromRgb(247, 86, 181));
                Exit_sm.Foreground = blond;
                Startyem.Background = new SolidColorBrush(Color.FromRgb(247, 86, 181));
                Startyem.Foreground = blond;
                support.Foreground = blond;
                support.Background = new SolidColorBrush(Color.FromRgb(247, 86, 181));
                RestartQuesstion.Background = new SolidColorBrush(Color.FromRgb(247, 86, 181));
                RestartQuesstion.Foreground =blond;
                GotoFinal.Background= new SolidColorBrush(Color.FromRgb(247, 86, 181));
                GotoFinal.Foreground=blond;
            }
            else
            {
                DarkMode.IsChecked = true;
                DarkMode_s.IsChecked = true;
                DarkMode_3td.IsChecked = true;
                DarkMode_final.IsChecked = true;
                //MessageBox.Show("on");
                foreach (System.Windows.Controls.Grid tb in TIDARK.Children.OfType<Grid>())
                {
                    tb.Background = new SolidColorBrush(Color.FromArgb(77, 77, 77, 80));
                    foreach (System.Windows.Controls.TextBox tb_text in tb.Children.OfType<TextBox>())
                    {
                        tb_text.Background = new SolidColorBrush(Color.FromRgb(77, 77, 77));
                        tb_text.Foreground = blond;
                    }
                }
                foreach (System.Windows.Controls.TextBox tb_text in PlatFinal.Children.OfType<TextBox>())
                {
                    tb_text.Background = new SolidColorBrush(Color.FromRgb(77, 77, 77));
                    tb_text.Foreground = blond;
                }
                Exit_ff.Background = dark;
                Exit_ff.Foreground = blond;
                Exit_3е.Background = dark;
                Exit_3е.Foreground = blond;
                Exit_Finish.Background = dark;
                Exit_Finish.Foreground = blond;
                RestartQuesstion.Background= dark;
                RestartQuesstion.Foreground= blond;
                Exit_sm.Background = dark;
                support.Foreground = blond;
                support.Background = dark;
                sometext.Background = new SolidColorBrush(Color.FromRgb(55, 55, 55));
                sometext.Foreground = blond;
                Exit_sm.Foreground = blond;
                Startyem.Background = dark;
                Startyem.Foreground = blond;
                Start_page.Background = dark;
                TIDARK.Background = dark;
                td3grid.Background = dark;
                PlatFinal.Background = new SolidColorBrush(Color.FromArgb(77, 77, 77, 80));
                TIfinal.Background = dark;
                DarkMode.Foreground = new SolidColorBrush(Color.FromRgb(255, 255, 255));
                DarkMode_s.Foreground = new SolidColorBrush(Color.FromRgb(255, 255, 255));
                ButtonActivacia.Background = dark;
                ButtonActivacia.Foreground = blond;
                GotoFinal.Background = dark;
                GotoFinal.Foreground =blond;
            }
        }//Темная тема, куда без неё
        void OtkryvayPortalVVosimoeObshezhitie()
        {
            var props = new ClassLib.Properties();
            int counter = 1;
            //Сначала проверяем на валидность
            foreach (System.Windows.Controls.Grid tb in TIDARK.Children.OfType<Grid>())
            {
                foreach (System.Windows.Controls.TextBox tb_text in tb.Children.OfType<TextBox>())
                {
                    try
                    {
                        double researchBadProps = Convert.ToDouble(tb_text.Text);
                    }
                    catch(Exception ex) {
                        tb_text.Background = new SolidColorBrush(Color.FromRgb(240, 32, 32));
                        counter++;
                    }
                } 
            }
            if (counter != 1)
            {
                MessageBox.Show("Введены некорректные параметры.");
                return;
            }


            //Константы
            double RecommendedFilterRegenerationTime = 60;
            double sterlingConstGasConst = 124;
            double sterlingConstAirConst = 124;
            double sterlingConstWaterConst = 961;
            //Константы кончились

            //Значения с табло
            double DustParticleDensityProps = Convert.ToDouble(DustParticleDensity.Text);
            double PlotnostComeingOutD = Convert.ToDouble(PlotnostComeingOutDust.Text);
            double AverageRazmerDustProps = Convert.ToDouble(AverageRazmerDust.Text);
            double rasHodGasProps = Convert.ToDouble(rasHodGas.Text);
            double SteamGasWhichContainsInProps = Convert.ToDouble(SteamGasWhichContainsIn.Text);
            double waterSteamWhichContainsInAtmosphereAirProps = Convert.ToDouble(waterSteamWhichContainsInAtmosphereAir.Text);
            double BarometricPressureGround_PbarProps = Convert.ToDouble(BarometricPressureGround_Pbar.Text);
            double temperatureComeOutGasForOchistkaProps = Convert.ToDouble(temperatureComeOutGasForOchistka.Text);
            double gasQUnderHisAllowedTemperatureProps = Convert.ToDouble(gasQUnderHisAllowedTemperature.Text);
            double allowedTemperatureWorkClearedGasProps = Convert.ToDouble(allowedTemperatureWorkClearedGas.Text);
            double gasQUnderHisTemperatureProps = Convert.ToDouble(gasQUnderHisTemperature.Text);
            double QAtmosphereAirUnderAllowedGasTemperatureProps = Convert.ToDouble(QAtmosphereAirUnderAllowedGasTemperature.Text);
            double airTemperatureProps = Convert.ToDouble(airTemperature.Text);
            double ExcessGasPressure_RgProps = Convert.ToDouble(ExcessGasPressure_Rg.Text);
            double gasViskosityNormalSituationProps = Convert.ToDouble(gasViskosityNormalSituation.Text);
            double FiltrationRateProps = Convert.ToDouble(FiltrationRate.Text);
            double allowedResistanceProps = Convert.ToDouble(allowedResistance.Text);
            double waterViskosityNormalSituationProps = Convert.ToDouble(waterViskosityNormalSituation.Text);
            double airViskosityNormalSituationProps = Convert.ToDouble(airViskosityNormalSituation.Text);
            double PlotnostiPriNormalnoBratishkaGasProps = Convert.ToDouble(PlotnostiPriNormalnoBratishkaGas.Text);
            double PlotnostiPriNormalnoBratishkaAirProps = Convert.ToDouble(PlotnostiPriNormalnoBratishkaAir.Text);
            double OptimalnyiVyhodGasProps = Convert.ToDouble(OptimalnyiVyhodGas.Text);
            double OptimalnyiResistanceProps = Convert.ToDouble(OptimalnyiResistance.Text);
            




            //Значения исходя из комбоксов
            Model_collection ForItems = new Model_collection(); // экземпляр объекта с табличными параметрами
            double standardGasFilterLoad = 0;
            //LinQ, Вытаскиваем данные выбранного объекта. Выглядит круто и делает код более продвинутым(наверно)
            var standard = ForItems.DustlType.Where(x => x.Name.Contains(typeDust.Text.ToString()));
            foreach (var dt in standard) //достаём данные выбранного объекта
            {
                double a = Convert.ToDouble(dt.Koeficient);
                standardGasFilterLoad = a;
            }
            var standardTkani = ForItems.MaterialType.Where(x => x.Name.Contains(MaterialType.Text.ToString()));
            double H0 = 0;
            double PoristostTkani = 0;
            foreach (var st in standardTkani) //достаём данные выбранного объекта
            {
                double a = Convert.ToDouble(st.H0); //присваиваем коэффициенты
                double c = Convert.ToDouble(st.Poristost); //присваиваем коэффициенты
                H0 = a;
                PoristostTkani = c;
            }
            //Значения исходя из комбобоксов закончились
            //Начались условия для определения времени регенерации (filtrationSpeedClass)
            double regenTime = 0;
            double Koefk1 = 0;
            int decrementParty = 0;
            foreach (var FiendKoef in ForItems.filtrationSpeed) //листаем список и ищем сходство между фильтром и регенерацией
            {
                if (FiendKoef.NameMaterial.ToString()==MaterialType.Text.ToString() && FiendKoef.TypeOfReg.ToString() == TypeOfRegeneration.Text.ToString())
                {
                    regenTime = FiendKoef.RegenerationTime;
                    Koefk1 = FiendKoef.KoefK;
                    decrementParty = 0;
                    break;
                }
                else //если соответствий не найдено
                {
                    decrementParty--;
                }
            }
            //Логика следующая: если мы не нашли совпадений, просто декрементируем переменную
            //при совпадении, обнуляем её и никогда не попадаем в цикл ниже.
            if (decrementParty < 0)
            {
                string Right = "";
                var Podhodyashii = ForItems.filtrationSpeed.Where(x => x.NameMaterial.Contains(MaterialType.Text.ToString()));
                foreach (var c in Podhodyashii)
                    Right = c.TypeOfReg;
                MessageBox.Show("неподходящий тип регенерации, попробуйте " + Right);
                return; //и выходим из метода(чтобы дальше не считать)
            }

            //Считаем по формуле метода
            double result = ClassLib.Properties.D29_Ff(H0, PoristostTkani, Koefk1, standardGasFilterLoad, RecommendedFilterRegenerationTime, DustParticleDensityProps, PlotnostComeingOutD, AverageRazmerDustProps, rasHodGasProps, SteamGasWhichContainsInProps, waterSteamWhichContainsInAtmosphereAirProps, BarometricPressureGround_PbarProps, temperatureComeOutGasForOchistkaProps, gasQUnderHisAllowedTemperatureProps, allowedTemperatureWorkClearedGasProps, gasQUnderHisTemperatureProps, QAtmosphereAirUnderAllowedGasTemperatureProps, airTemperatureProps, ExcessGasPressure_RgProps, gasViskosityNormalSituationProps, sterlingConstGasConst, airViskosityNormalSituationProps, sterlingConstAirConst, waterViskosityNormalSituationProps, sterlingConstWaterConst, FiltrationRateProps);
            isResult.Text=result.ToString(".####"); //Math.Round(js) в c# не помню как называется этот метод, но суть одна
            XmlSerializer formatter = new XmlSerializer(typeof(Serialize));
            using (FileStream Fs = new FileStream("ForSomeText.txt", FileMode.Create))
            {
                Serialize serik = new Serialize();
                serik.DustParticleDensityProps = DustParticleDensityProps;
                serik.PlotnostComeingOutD = PlotnostComeingOutD;
                serik.AverageRazmerDustProps = AverageRazmerDustProps;
                serik.rasHodGasProps = rasHodGasProps;
                serik.SteamGasWhichContainsInProps = SteamGasWhichContainsInProps;
                serik.waterSteamWhichContainsInAtmosphereAirProps = waterSteamWhichContainsInAtmosphereAirProps;
                serik.BarometricPressureGround_PbarProps = BarometricPressureGround_PbarProps;
                serik.temperatureComeOutGasForOchistkaProps = temperatureComeOutGasForOchistkaProps;
                serik.gasQUnderHisAllowedTemperatureProps = gasQUnderHisAllowedTemperatureProps;
                serik.allowedTemperatureWorkClearedGasProps = allowedTemperatureWorkClearedGasProps;
                serik.gasQUnderHisTemperatureProps = gasQUnderHisTemperatureProps;
                serik.QAtmosphereAirUnderAllowedGasTemperatureProps = QAtmosphereAirUnderAllowedGasTemperatureProps;
                serik.airTemperatureProps = airTemperatureProps;
                serik.ExcessGasPressure_RgProps = ExcessGasPressure_RgProps;
                serik.gasViskosityNormalSituationProps = gasViskosityNormalSituationProps;
                serik.FiltrationRateProps = FiltrationRateProps;
                serik.waterViskosityNormalSituationProps = waterViskosityNormalSituationProps;
                serik.airViskosityNormalSituationProps = airViskosityNormalSituationProps;
                serik.PlotnostiPriNormalnoBratishkaAirProps = PlotnostiPriNormalnoBratishkaAirProps;
                serik.PlotnostiPriNormalnoBratishkaGasProps = PlotnostiPriNormalnoBratishkaGasProps;
                serik.OptimalnyiVyhodGasProps = OptimalnyiVyhodGasProps;
                serik.OptimalnyiResistanceProps = OptimalnyiResistanceProps;
                serik.allowedResistanceProps = allowedResistanceProps;


                formatter.Serialize(Fs, serik);               
            }//сериализация в файлик

            focusType.IsEnabled = true;
            focusType.Focus();
        }//подсчёт фильтрующей поверхности
        void GotoStart()
        {
            CalcForm.IsSelected = true;
            focusType.IsEnabled = false;
            Final_ca.IsEnabled = false;
        }
        void ListenerSetter(Model_collection mc)
        {
            DarkMode.Click += (s, e) => { DarkModeIsReal(DarkMode.IsChecked); };
            DarkMode_s.Click += (s, e) => { DarkModeIsReal(DarkMode_s.IsChecked); };
            DarkMode_3td.Click += (s, e) => { DarkModeIsReal(DarkMode_3td.IsChecked); };
            DarkMode_final.Click += (s, e) => { DarkModeIsReal(DarkMode_final.IsChecked); };
            ButtonActivacia.Click += (s, e) => { OtkryvayPortalVVosimoeObshezhitie(); };
            Startyem.Click += (s, e) => { CalcForm.IsEnabled = true; CalcForm.IsSelected = true; };
            RestartQuesstion.Click += (acc, send) => { GotoStart(); };
            Exit_sm.Click += (s, e) => { this.Close(); };
            Exit_ff.Click += (s, e) => { this.Close(); };
            Exit_Finish.Click += (s, e) => { this.Close(); };
            Exit_3е.Click += (s, e) => { this.Close(); };
            FiltrFind.SelectionChanged += (s, e) => { FiltAlgoritm(mc); };
            GotoFinal.Click += (s, e) => { Final(mc); };
            Reportislav.Click += (s, e) => { CallReport(); };
            support.Click += (s, e) => { Help hp = new Help(); hp.ShowDialog(); };
        }
        void CallReport()
        {
            OknosReportom or = new OknosReportom(rListforReport);
            or.ShowDialog();
        }
        public MainWindow()
        {
            InitializeComponent();
            SerializeToBoxes(); //забираем данные с файлика
            Model_collection mc = new Model_collection();
            DataContext = mc;
            FiltAlgoritm(mc);
            ListenerSetter(mc); //вешаем события
            
        }
        void FiltAlgoritm(Model_collection mc)
        {
            FiltrFind.ItemsSource = mc.NaimenovaniaFiltrov;
            FiltrFind.DisplayMemberPath = "NameF";
            var Podhodyashii = mc.FilterType.Where(x => x.idF.Equals(FiltrFind.SelectedIndex));
            if (Podhodyashii.Count() != 0)
            {
                filtry.ItemsSource = Podhodyashii;
            }
           
        }
        void Final(Model_collection mc)
        {
            //Константы
            double RecommendedFilterRegenerationTime = 60;
            double sterlingConstGasConst = 124;
            double sterlingConstAirConst = 124;
            double sterlingConstWaterConst = 961;
            //Константы кончились

            //Значения с табло
            double DustParticleDensityProps = Convert.ToDouble(DustParticleDensity.Text);
            double PlotnostComeingOutD = Convert.ToDouble(PlotnostComeingOutDust.Text);
            double AverageRazmerDustProps = Convert.ToDouble(AverageRazmerDust.Text);
            double rashodDryClearedGasForNornalSituationProps = Convert.ToDouble(rasHodGas.Text);
            double SteamGasWhichContainsInProps = Convert.ToDouble(SteamGasWhichContainsIn.Text);
            double waterSteamWhichContainsInAtmosphereAirProps = Convert.ToDouble(waterSteamWhichContainsInAtmosphereAir.Text);
            double BarometricPressureGround_PbarProps = Convert.ToDouble(BarometricPressureGround_Pbar.Text);
            double temperatureComeOutGasForOchistkaProps = Convert.ToDouble(temperatureComeOutGasForOchistka.Text);
            double gasQUnderHisAllowedTemperatureProps = Convert.ToDouble(gasQUnderHisAllowedTemperature.Text);
            double allowedTemperatureWorkClearedGasProps = Convert.ToDouble(allowedTemperatureWorkClearedGas.Text);
            double gasQUnderHisTemperatureProps = Convert.ToDouble(gasQUnderHisTemperature.Text);
            double QAtmosphereAirUnderAllowedGasTemperatureProps = Convert.ToDouble(QAtmosphereAirUnderAllowedGasTemperature.Text);
            double airTemperatureProps = Convert.ToDouble(airTemperature.Text);
            double ExcessGasPressure_RgProps = Convert.ToDouble(ExcessGasPressure_Rg.Text);
            double gasViskosityNormalSituationProps = Convert.ToDouble(gasViskosityNormalSituation.Text);
            double FiltrationRateProps = Convert.ToDouble(FiltrationRate.Text);
            double waterViskosityNormalSituationProps = Convert.ToDouble(waterViskosityNormalSituation.Text);
            double airViskosityNormalSituationProps = Convert.ToDouble(airViskosityNormalSituation.Text);
            double OptimalnyiVyhodGasProps = Convert.ToDouble(OptimalnyiVyhodGas.Text);
            double OptimalnyiResistanceProps = Convert.ToDouble(OptimalnyiResistance.Text);
            double PlotnostiPriNormalnoBratishkaGasProps = Convert.ToDouble(PlotnostiPriNormalnoBratishkaGas.Text);
            double PlotnostiPriNormalnoBratishkaAirProps = Convert.ToDouble(PlotnostiPriNormalnoBratishkaAir.Text);
            Model_collection ForItems = new Model_collection(); // экземпляр объекта с табличными параметрами
            double standardGasFilterLoad = 0;
            //LinQ, Вытаскиваем данные выбранного объекта. Выглядит круто и делает код более продвинутым(наверно)
            var standard = ForItems.DustlType.Where(x => x.Name.Contains(typeDust.Text.ToString()));
            foreach (var dt in standard) //достаём данные выбранного объекта
            {
                double aFind = Convert.ToDouble(dt.Koeficient);
                standardGasFilterLoad = aFind;
            }
            var standardTkani = ForItems.MaterialType.Where(x => x.Name.Contains(MaterialType.Text.ToString()));
            double H0 = 0;
            double PoristostTkani = 0;
            foreach (var st in standardTkani) //достаём данные выбранного объекта
            {
                double ache = Convert.ToDouble(st.H0); //присваиваем коэффициенты
                double c = Convert.ToDouble(st.Poristost); //присваиваем коэффициенты
                H0 = ache;
                PoristostTkani = c;
            }
            double regenTime = 0;
            double Koefk1 = 0;
            int decrementParty = 0;
            foreach (var FiendKoef in ForItems.filtrationSpeed) //листаем список и ищем сходство между фильтром и регенерацией
            {
                if (FiendKoef.NameMaterial.ToString() == MaterialType.Text.ToString() && FiendKoef.TypeOfReg.ToString() == TypeOfRegeneration.Text.ToString())
                {
                    regenTime = FiendKoef.RegenerationTime;
                    Koefk1 = FiendKoef.KoefK;
                    decrementParty = 0;
                    break;
                }
                else //если соответствий не найдено
                {
                    decrementParty--;
                }
            }
            //корректировка свойств
            double DustConcentrationInWasteGasProps = PlotnostComeingOutD;
            double waterSteamWhichContainsInOutComingGasProps = SteamGasWhichContainsInProps;
            double D36_Ploshad_filtracii = 0;
            double EntHandsProps = 0;
            var a = filtry.SelectedIndex;
            var Podhodyashii = mc.FilterType.Where(x => x.idF.Equals(FiltrFind.SelectedIndex));
            int stop = 0;
            //ух капец, вытаскиваем площадь из дата грида p.s. по другому вообще без понятия как это делать (селекторы не хотят адекватно работать)
            foreach(var counter in Podhodyashii)
            {
                D36_Ploshad_filtracii = counter.FiltrSquare;
                EntHandsProps = counter.EntHands;
                if (filtry.SelectedIndex == stop)
                    break;
                stop++;
            }
            if (Convert.ToDouble(isResult.Text) >= D36_Ploshad_filtracii) //выбрасываем ерорчик
            {
                MessageBox.Show($"Фильтрующая поверхность {D36_Ploshad_filtracii} должна быть больше ориентировочной площади {isResult.Text}", "Ошибка, товаришь");
                return; //ну и дальше не идём 
            }
            //если всё прекрасно, едем дальше
            FullSopromat.Text = Math.Round(ClassLib.Properties.D24_Δpк(OptimalnyiResistanceProps, OptimalnyiVyhodGasProps, BarometricPressureGround_PbarProps, waterSteamWhichContainsInAtmosphereAirProps, allowedTemperatureWorkClearedGasProps, ExcessGasPressure_RgProps, PlotnostiPriNormalnoBratishkaGasProps, rashodDryClearedGasForNornalSituationProps, temperatureComeOutGasForOchistkaProps, PlotnostiPriNormalnoBratishkaAirProps, waterSteamWhichContainsInAtmosphereAirProps, gasQUnderHisTemperatureProps, gasQUnderHisTemperatureProps, QAtmosphereAirUnderAllowedGasTemperatureProps, airTemperatureProps, gasQUnderHisAllowedTemperatureProps), 5).ToString();
            double RaschetTimichaText = ClassLib.Properties.D26(H0, PoristostTkani,DustParticleDensityProps, PlotnostComeingOutD,AverageRazmerDustProps, rashodDryClearedGasForNornalSituationProps, waterSteamWhichContainsInAtmosphereAirProps, waterSteamWhichContainsInAtmosphereAirProps, BarometricPressureGround_PbarProps, temperatureComeOutGasForOchistkaProps, gasQUnderHisAllowedTemperatureProps, allowedTemperatureWorkClearedGasProps,gasQUnderHisTemperatureProps,QAtmosphereAirUnderAllowedGasTemperatureProps, airTemperatureProps, ExcessGasPressure_RgProps, gasViskosityNormalSituationProps, sterlingConstGasConst, airViskosityNormalSituationProps,sterlingConstAirConst, waterViskosityNormalSituationProps,sterlingConstWaterConst,FiltrationRateProps);
            RaschetTimicha.Text = RaschetTimichaText.ToString("#.###");
            double QuantityDustNaPoverhnostyText = ClassLib.Properties.D38(H0, PoristostTkani, DustParticleDensityProps, PlotnostComeingOutD, AverageRazmerDustProps, rashodDryClearedGasForNornalSituationProps, waterSteamWhichContainsInAtmosphereAirProps, waterSteamWhichContainsInAtmosphereAirProps, BarometricPressureGround_PbarProps, temperatureComeOutGasForOchistkaProps, gasQUnderHisAllowedTemperatureProps, allowedTemperatureWorkClearedGasProps, gasQUnderHisTemperatureProps, QAtmosphereAirUnderAllowedGasTemperatureProps, airTemperatureProps, ExcessGasPressure_RgProps, gasViskosityNormalSituationProps, sterlingConstGasConst, airViskosityNormalSituationProps, sterlingConstAirConst, waterViskosityNormalSituationProps, sterlingConstWaterConst, FiltrationRateProps);
            QuantityDustNaPoverhnosty.Text = QuantityDustNaPoverhnostyText.ToString("#.###");
            double QuantityRegPerHourText = ClassLib.Properties.D27(H0, PoristostTkani, RecommendedFilterRegenerationTime, DustParticleDensityProps, PlotnostComeingOutD, AverageRazmerDustProps, rashodDryClearedGasForNornalSituationProps, SteamGasWhichContainsInProps, waterSteamWhichContainsInAtmosphereAirProps,BarometricPressureGround_PbarProps, temperatureComeOutGasForOchistkaProps, gasQUnderHisAllowedTemperatureProps, allowedTemperatureWorkClearedGasProps, gasQUnderHisTemperatureProps, QAtmosphereAirUnderAllowedGasTemperatureProps,airTemperatureProps, ExcessGasPressure_RgProps, gasViskosityNormalSituationProps, sterlingConstGasConst, airViskosityNormalSituationProps,sterlingConstAirConst, waterViskosityNormalSituationProps, sterlingConstWaterConst,FiltrationRateProps);
            RaschetnoeZaChas.Text = QuantityRegPerHourText.ToString("#.###");
             double GazNaOchistkyText = ClassLib.Properties.D28_Vp(H0, PoristostTkani, RecommendedFilterRegenerationTime, DustParticleDensityProps, DustConcentrationInWasteGasProps, AverageRazmerDustProps, rashodDryClearedGasForNornalSituationProps, waterSteamWhichContainsInOutComingGasProps, waterSteamWhichContainsInAtmosphereAirProps, BarometricPressureGround_PbarProps, temperatureComeOutGasForOchistkaProps, gasQUnderHisTemperatureProps, allowedTemperatureWorkClearedGasProps, gasQUnderHisTemperatureProps, QAtmosphereAirUnderAllowedGasTemperatureProps, airTemperatureProps, ExcessGasPressure_RgProps, gasViskosityNormalSituationProps, sterlingConstGasConst, airViskosityNormalSituationProps, sterlingConstAirConst, waterViskosityNormalSituationProps, sterlingConstWaterConst, FiltrationRateProps);
            GazNaOchistky.Text = GazNaOchistkyText.ToString("#.###");
           double FiltrSquareText = ClassLib.Properties.D29_Ff(H0, PoristostTkani, Koefk1, standardGasFilterLoad, RecommendedFilterRegenerationTime, DustParticleDensityProps, PlotnostComeingOutD, AverageRazmerDustProps, rashodDryClearedGasForNornalSituationProps, SteamGasWhichContainsInProps, waterSteamWhichContainsInAtmosphereAirProps, BarometricPressureGround_PbarProps, temperatureComeOutGasForOchistkaProps, gasQUnderHisAllowedTemperatureProps, allowedTemperatureWorkClearedGasProps, gasQUnderHisTemperatureProps, QAtmosphereAirUnderAllowedGasTemperatureProps, airTemperatureProps, ExcessGasPressure_RgProps, gasViskosityNormalSituationProps, sterlingConstGasConst, airViskosityNormalSituationProps, sterlingConstAirConst, waterViskosityNormalSituationProps, sterlingConstWaterConst, FiltrationRateProps);
            FiltrSquare.Text = FiltrSquareText.ToString("#.###");
            double OneSectionSquareText = ClassLib.Properties.D30(D36_Ploshad_filtracii, EntHandsProps);
            OneSectionSquare.Text = OneSectionSquareText.ToString("#.###");
            double FiltPoverhnostDuringRegOffText = ClassLib.Properties.D31(H0, PoristostTkani, D36_Ploshad_filtracii, RecommendedFilterRegenerationTime, DustParticleDensityProps, DustConcentrationInWasteGasProps, AverageRazmerDustProps, rashodDryClearedGasForNornalSituationProps, waterSteamWhichContainsInOutComingGasProps, waterSteamWhichContainsInAtmosphereAirProps, BarometricPressureGround_PbarProps, temperatureComeOutGasForOchistkaProps, gasQUnderHisTemperatureProps, allowedTemperatureWorkClearedGasProps, gasQUnderHisTemperatureProps, QAtmosphereAirUnderAllowedGasTemperatureProps, airTemperatureProps, ExcessGasPressure_RgProps, gasViskosityNormalSituationProps, sterlingConstGasConst, airViskosityNormalSituationProps, sterlingConstAirConst, waterViskosityNormalSituationProps, sterlingConstWaterConst, FiltrationRateProps);
            FiltPoverhnostDuringRegOff.Text = FiltPoverhnostDuringRegOffText.ToString("#.###");
             double YtochnennoeQuanAirForProduvkaText = ClassLib.Properties.D32(H0,PoristostTkani, D36_Ploshad_filtracii, RecommendedFilterRegenerationTime, DustParticleDensityProps, DustConcentrationInWasteGasProps, AverageRazmerDustProps, rashodDryClearedGasForNornalSituationProps, waterSteamWhichContainsInOutComingGasProps, waterSteamWhichContainsInAtmosphereAirProps, BarometricPressureGround_PbarProps, temperatureComeOutGasForOchistkaProps, gasQUnderHisTemperatureProps, allowedTemperatureWorkClearedGasProps, gasQUnderHisTemperatureProps, QAtmosphereAirUnderAllowedGasTemperatureProps, airTemperatureProps, ExcessGasPressure_RgProps, gasViskosityNormalSituationProps, sterlingConstGasConst, airViskosityNormalSituationProps, sterlingConstAirConst, waterViskosityNormalSituationProps, sterlingConstWaterConst, FiltrationRateProps);
            YtochnennoeQuanAirForProduvka.Text = YtochnennoeQuanAirForProduvkaText.ToString("#.###");
            double YtochnennayPloshadText = ClassLib.Properties.D33(H0,PoristostTkani,Koefk1, standardGasFilterLoad, D36_Ploshad_filtracii,RecommendedFilterRegenerationTime,DustParticleDensityProps, DustConcentrationInWasteGasProps, AverageRazmerDustProps,rashodDryClearedGasForNornalSituationProps, waterSteamWhichContainsInOutComingGasProps,waterSteamWhichContainsInAtmosphereAirProps,BarometricPressureGround_PbarProps, temperatureComeOutGasForOchistkaProps,gasQUnderHisAllowedTemperatureProps, allowedTemperatureWorkClearedGasProps, gasQUnderHisTemperatureProps, QAtmosphereAirUnderAllowedGasTemperatureProps, airTemperatureProps, ExcessGasPressure_RgProps, gasViskosityNormalSituationProps, sterlingConstGasConst, airViskosityNormalSituationProps,sterlingConstAirConst, waterViskosityNormalSituationProps, sterlingConstWaterConst, FiltrationRateProps);
            YtochnennayPloshad.Text = YtochnennayPloshadText.ToString("#.###");
            double OtlicchieNormAndNeNormText = ClassLib.Properties.D34(H0, PoristostTkani, Koefk1, standardGasFilterLoad, D36_Ploshad_filtracii, RecommendedFilterRegenerationTime, DustParticleDensityProps, DustConcentrationInWasteGasProps, AverageRazmerDustProps, rashodDryClearedGasForNornalSituationProps, waterSteamWhichContainsInOutComingGasProps, waterSteamWhichContainsInAtmosphereAirProps, BarometricPressureGround_PbarProps, temperatureComeOutGasForOchistkaProps, gasQUnderHisAllowedTemperatureProps, allowedTemperatureWorkClearedGasProps, gasQUnderHisTemperatureProps, QAtmosphereAirUnderAllowedGasTemperatureProps, airTemperatureProps, ExcessGasPressure_RgProps, gasViskosityNormalSituationProps, sterlingConstGasConst, airViskosityNormalSituationProps, sterlingConstAirConst, waterViskosityNormalSituationProps, sterlingConstWaterConst, FiltrationRateProps);
            OtlicchieNormAndNeNorm.Text = OtlicchieNormAndNeNormText.ToString("#.###");
             double FacticheskayNagryzkaText = ClassLib.Properties.D36(Koefk1, EntHandsProps, AverageRazmerDustProps, standardGasFilterLoad, Koefk1, DustConcentrationInWasteGasProps, rashodDryClearedGasForNornalSituationProps, waterSteamWhichContainsInOutComingGasProps, waterSteamWhichContainsInAtmosphereAirProps, BarometricPressureGround_PbarProps, temperatureComeOutGasForOchistkaProps, gasQUnderHisTemperatureProps, allowedTemperatureWorkClearedGasProps, gasQUnderHisTemperatureProps, QAtmosphereAirUnderAllowedGasTemperatureProps, airTemperatureProps, ExcessGasPressure_RgProps);
            FacticheskayNagryzka.Text = FacticheskayNagryzkaText.ToString("#.###");
            double QuantDustOnOneSectionText = ClassLib.Properties.D39(H0, PoristostTkani, EntHandsProps, D36_Ploshad_filtracii, DustParticleDensityProps, DustConcentrationInWasteGasProps, AverageRazmerDustProps, rashodDryClearedGasForNornalSituationProps, waterSteamWhichContainsInOutComingGasProps, waterSteamWhichContainsInAtmosphereAirProps, BarometricPressureGround_PbarProps, temperatureComeOutGasForOchistkaProps, gasQUnderHisTemperatureProps, allowedTemperatureWorkClearedGasProps, gasQUnderHisTemperatureProps, QAtmosphereAirUnderAllowedGasTemperatureProps, airTemperatureProps, ExcessGasPressure_RgProps, gasViskosityNormalSituationProps, sterlingConstGasConst, airViskosityNormalSituationProps, sterlingConstAirConst, waterViskosityNormalSituationProps, sterlingConstWaterConst, FiltrationRateProps);
            QuantDustOnOneSection.Text = QuantDustOnOneSectionText.ToString("#.###");
            Final_ca.IsEnabled = true;
            Final_ca.IsSelected = true;
            ReportCreator(PoristostTkani.ToString(), Koefk1.ToString(), sterlingConstGasConst.ToString(), sterlingConstAirConst.ToString(), sterlingConstWaterConst.ToString(), RecommendedFilterRegenerationTime.ToString());
        } //результаты и всё всё всё
        void SerializeToBoxes() //вывод данных из коробки сериализации
        {
            Serialize serik = new Serialize();
            XmlSerializer formatter = new XmlSerializer(typeof(Serialize));
            try
            {
                using (FileStream Fs = new FileStream("ForSomeText.txt", FileMode.Open))
                {
                    serik = (Serialize)formatter.Deserialize(Fs);
                }
                DustParticleDensity.Text = serik.DustParticleDensityProps.ToString();
                PlotnostComeingOutDust.Text = serik.PlotnostComeingOutD.ToString();
                AverageRazmerDust.Text = serik.AverageRazmerDustProps.ToString();
                rasHodGas.Text = serik.rasHodGasProps.ToString();
                SteamGasWhichContainsIn.Text = serik.SteamGasWhichContainsInProps.ToString();
                waterSteamWhichContainsInAtmosphereAir.Text = serik.waterSteamWhichContainsInAtmosphereAirProps.ToString();
                BarometricPressureGround_Pbar.Text = serik.BarometricPressureGround_PbarProps.ToString();
                temperatureComeOutGasForOchistka.Text = serik.temperatureComeOutGasForOchistkaProps.ToString();
                gasQUnderHisAllowedTemperature.Text = serik.gasQUnderHisAllowedTemperatureProps.ToString();
                allowedTemperatureWorkClearedGas.Text = serik.allowedTemperatureWorkClearedGasProps.ToString();
                gasQUnderHisTemperature.Text = serik.gasQUnderHisTemperatureProps.ToString();
                QAtmosphereAirUnderAllowedGasTemperature.Text = serik.QAtmosphereAirUnderAllowedGasTemperatureProps.ToString();
                airTemperature.Text = serik.airTemperatureProps.ToString();
                ExcessGasPressure_Rg.Text = serik.ExcessGasPressure_RgProps.ToString();
                gasViskosityNormalSituation.Text = serik.gasViskosityNormalSituationProps.ToString();
                FiltrationRate.Text = serik.FiltrationRateProps.ToString();
                waterViskosityNormalSituation.Text = serik.waterViskosityNormalSituationProps.ToString();
                airViskosityNormalSituation.Text = serik.airViskosityNormalSituationProps.ToString();
                PlotnostiPriNormalnoBratishkaAir.Text = serik.PlotnostiPriNormalnoBratishkaAirProps.ToString();
                PlotnostiPriNormalnoBratishkaGas.Text = serik.PlotnostiPriNormalnoBratishkaGasProps.ToString();
                OptimalnyiVyhodGas.Text = serik.OptimalnyiVyhodGasProps.ToString();
                OptimalnyiResistance.Text = serik.OptimalnyiResistanceProps.ToString();
                allowedResistance.Text = serik.allowedResistanceProps.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Похоже вы используете программу первый раз");
            }
            



        }
        void ReportCreator(string poristTk, string kKoef, string sterlgas, string sterlair, string sterlwater, string recomTime)
        {
            rListforReport.Add(new ReportClass { Name = "плотность пылевых частиц кг/м3", Value = DustParticleDensity.Text });
            rListforReport.Add(new ReportClass { Name = "средний медианный диаметр пылевых частиц мкм", Value = AverageRazmerDust.Text });
            rListforReport.Add(new ReportClass { Name = "расход сухого очищаемого газа м3/ч", Value = rasHodGas.Text });
            rListforReport.Add(new ReportClass { Name = "содержание водяных паров в отходящих газах кг/м3", Value = SteamGasWhichContainsIn.Text });
            rListforReport.Add(new ReportClass { Name = "содержание водяных паров в воздухе кг/м3", Value = waterSteamWhichContainsInAtmosphereAir.Text });
            rListforReport.Add(new ReportClass { Name = "барометрическое давление на местности кПа", Value = BarometricPressureGround_Pbar.Text });
            rListforReport.Add(new ReportClass { Name = "температура отходящего на очистку газа °С", Value = temperatureComeOutGasForOchistka.Text });
            rListforReport.Add(new ReportClass { Name = "допустимая рабочая температура очищаемого газа °С", Value = allowedTemperatureWorkClearedGas.Text });
            rListforReport.Add(new ReportClass { Name = "температура воздуха °С", Value = airTemperature.Text });
            rListforReport.Add(new ReportClass { Name = "избыточное давление газа кПа", Value = ExcessGasPressure_Rg.Text });
            rListforReport.Add(new ReportClass { Name = "скорость фильтрации м/мин", Value = FiltrationRate.Text });
            rListforReport.Add(new ReportClass { Name = "оптимальная скорость газа на выходном патрубке фильтре м/с", Value = OptimalnyiVyhodGas.Text });
            rListforReport.Add(new ReportClass { Name = "коэффициент сопротивления входного патрубка и корпуса фильтра", Value = OptimalnyiResistance.Text });
            rListforReport.Add(new ReportClass { Name = "плотность газа при нормальных условиях кг/м3", Value = PlotnostiPriNormalnoBratishkaGas.Text });
            rListforReport.Add(new ReportClass { Name = "плотность воздуха при нормальных условиях кг/м3", Value = PlotnostiPriNormalnoBratishkaAir.Text });
            rListforReport.Add(new ReportClass { Name = "Рекомендованное время фильтрации", Value = recomTime });
            rListforReport.Add(new ReportClass { Name = "Тип фильтрующего материала", Value = MaterialType.Text });
            rListforReport.Add(new ReportClass { Name = "Способ регенерации", Value = TypeOfRegeneration.Text });
            rListforReport.Add(new ReportClass { Name = "Тип пыли", Value = typeDust.Text });
            rListforReport.Add(new ReportClass { Name = "Пористость Ткани", Value = poristTk });
            rListforReport.Add(new ReportClass { Name = "Коефициент К для Ткани", Value = kKoef });
            rListforReport.Add(new ReportClass { Name = "", Value = "" });
            rListforReport.Add(new ReportClass { Name = "Константы стерлинга", Value = "" });
            rListforReport.Add(new ReportClass { Name = "", Value = "" });
            rListforReport.Add(new ReportClass { Name = "Газа", Value = sterlgas });
            rListforReport.Add(new ReportClass { Name = "Воздуха", Value = sterlair });
            rListforReport.Add(new ReportClass { Name = "", Value = "" });
            rListforReport.Add(new ReportClass { Name = "", Value = "" });
            rListforReport.Add(new ReportClass { Name = "Коэффициенты динамической вязкости", Value = "" });
            rListforReport.Add(new ReportClass { Name = "", Value = "" });
            rListforReport.Add(new ReportClass { Name = "Газа", Value = gasViskosityNormalSituation.Text });
            rListforReport.Add(new ReportClass { Name = "Воздуха", Value = airViskosityNormalSituation.Text });
            rListforReport.Add(new ReportClass { Name = "Воды", Value = waterViskosityNormalSituation.Text });
            rListforReport.Add(new ReportClass { Name = "", Value = "" });
            rListforReport.Add(new ReportClass { Name = "Теплоёмкости", Value = "" });
            rListforReport.Add(new ReportClass { Name = "", Value = "" });
            rListforReport.Add(new ReportClass { Name = "Газа при норм. условиях", Value = gasQUnderHisAllowedTemperature.Text });
            rListforReport.Add(new ReportClass { Name = "Газа допустимая", Value = gasQUnderHisTemperature.Text });
            rListforReport.Add(new ReportClass { Name = "Атмосферного воздуха", Value = QAtmosphereAirUnderAllowedGasTemperature.Text });
            rListforReport.Add(new ReportClass { Name = "", Value = "" });
            rListforReport.Add(new ReportClass { Name = "Расчёты", Value = "" });
            rListforReport.Add(new ReportClass { Name = "", Value = "" });
            rListforReport.Add(new ReportClass { Name = "Полное гидравлическое сопротивление корпуса фильтра", Value = FullSopromat.Text });
            rListforReport.Add(new ReportClass { Name = "Расчетное время процесса фильтрации", Value = RaschetTimicha.Text });
            rListforReport.Add(new ReportClass { Name = "Количество пыли на поверхности фильтра", Value = QuantityDustNaPoverhnosty.Text });
            rListforReport.Add(new ReportClass { Name = "Расчетное количество регенераций фильтра за 1 час работы", Value = RaschetnoeZaChas.Text });
            rListforReport.Add(new ReportClass { Name = "Ориентировочное количество воздуха, идущего на продувку", Value = GazNaOchistky.Text });
            rListforReport.Add(new ReportClass { Name = "Ориентировочная площадь фильтрации", Value = FiltrSquare.Text });
            rListforReport.Add(new ReportClass { Name = "Площадь одной секции", Value = OneSectionSquare.Text });
            rListforReport.Add(new ReportClass { Name = "Фильтрущая поверхность, отключаемая на регенерацию", Value = FiltPoverhnostDuringRegOff.Text });
            rListforReport.Add(new ReportClass { Name = "Уточненное количество воздуха, идущего на продувку", Value = YtochnennoeQuanAirForProduvka.Text });
            rListforReport.Add(new ReportClass { Name = "Уточненная площадь фильтрации", Value = YtochnennayPloshad.Text });
            rListforReport.Add(new ReportClass { Name = "Отличие нормативной и уточненной площади фильтрации", Value = OtlicchieNormAndNeNorm.Text });
            rListforReport.Add(new ReportClass { Name = "Фактическая газовая нагрузка фильтра", Value = FacticheskayNagryzka.Text });
            rListforReport.Add(new ReportClass { Name = "Количество пыли накапливающейся на секции фильтра", Value = QuantDustOnOneSection.Text });



        }// строим отчёт
    }
}
