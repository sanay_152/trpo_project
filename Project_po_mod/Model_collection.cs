﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Project_po_mod
{
    public class Model_collection
    {
        public List<MaterialTypeClass> MaterialType { get; set; }
        public List<DustTypeClass> DustlType { get; set; }
        public List<TypeOfRegenerationClass> TypeOfRegeneration { get; set; }
        public List<filtrationSpeedClass> filtrationSpeed { get; set; }
        public List<FilterTypeClass> FilterType { get; set; }
        public List<NaimenovaniaFiltrovClass> NaimenovaniaFiltrov { get; set; }
        public Model_collection()
        {
            NaimenovaniaFiltrov = new List<NaimenovaniaFiltrovClass>()
            {
                new NaimenovaniaFiltrovClass { NameF="Фильтры рукавные с обратной продуыкой типа ФРО", id=0},
                new NaimenovaniaFiltrovClass { NameF="Фильтры типа ФРКИ (рукавные, каркасные с импульсной продувкой)", id=1},
                new NaimenovaniaFiltrovClass { NameF="Фильтры типа ФРКДИ (Рукавные, каркасные с двусторонней импульсной продувкой",id=2},
                new NaimenovaniaFiltrovClass { NameF="Фильтры типа СМЦ-101А (регенерация обратной продувкой", id=3},
                new NaimenovaniaFiltrovClass { NameF="Фильтры типа УРФМ (регенерация обратной продувкой с встряхиванием)", id = 4},
                new NaimenovaniaFiltrovClass { NameF="Фильтры типа ФРОС (регенерация обратной продувкой сжатым воздухом при Р = 0,15 + 0,20 МПа)", id = 5},
                new NaimenovaniaFiltrovClass { NameF="Фильтры типа РФСП (регенерация поэлементной струйной продувкой)", id=6},
                new NaimenovaniaFiltrovClass { NameF="Фильтры ФКИ (кассетные импульсные)", id=7},
            };
            MaterialType = new List<MaterialTypeClass>() {
                new MaterialTypeClass { Name = "Стеклоткань", H0 = 2700, Poristost = 0.52 },
                new MaterialTypeClass { Name = "Лавсан", H0=189, Poristost=0.75  },
                new MaterialTypeClass { Name = "Нетканый материал", H0=9200, Poristost= 0.35 },
                new MaterialTypeClass { Name = "Шерсть", H0 = 84, Poristost= 0.88 },
                new MaterialTypeClass { Name = "Нитрон", H0=180, Poristost= 0.83},
                new MaterialTypeClass { Name = "Полифен", H0 = 880, Poristost =0.66 },
            };
            DustlType = new List<DustTypeClass>()
            {
                new DustTypeClass { Name = "пыль антрацитного угля, возгонов металла", Koeficient = 1.5},
                new DustTypeClass { Name = "пыль кокса, золы, металла порошков, окислов металлов", Koeficient=1.7}
            };
            TypeOfRegeneration = new List<TypeOfRegenerationClass>()
            {
            new TypeOfRegenerationClass { Name = "Обратная продувка"},
            new TypeOfRegenerationClass { Name = "Обратная продувка со встряхиванием"},
            new TypeOfRegenerationClass { Name = "Импульсная продувка"},
            new TypeOfRegenerationClass { Name = "Струйная продувка"}
            };
            filtrationSpeed = new List<filtrationSpeedClass>()
            {
                new filtrationSpeedClass { NameMaterial = "Стеклоткань", TypeOfReg = "Обратная продувка", RegenerationTime = 0.5, KoefK=0.63},
                new filtrationSpeedClass { NameMaterial = "Лавсан", TypeOfReg = "Обратная продувка со встряхиванием", RegenerationTime = 0.7, KoefK = 0.84},
                new filtrationSpeedClass { NameMaterial = "Нетканый материал", TypeOfReg = "Импульсная продувка", RegenerationTime = 2.05, KoefK = 1},
                new filtrationSpeedClass { NameMaterial = "Лавсан", TypeOfReg = "Импульсная продувка", RegenerationTime = 2.05, KoefK = 1},
                new filtrationSpeedClass { NameMaterial = "Нетканый материал", TypeOfReg = "Струйная продувка", RegenerationTime = 4.5, KoefK = 1.1},
                new filtrationSpeedClass { NameMaterial = "Шерсть", TypeOfReg = "Обратная продувка со встряхиванием", RegenerationTime = 0.8, KoefK = 0.84},
                new filtrationSpeedClass { NameMaterial = "Нитрон", TypeOfReg = "Струйная продувка", RegenerationTime = 4.5, KoefK = 1.1},
                new filtrationSpeedClass { NameMaterial = "Полифен", TypeOfReg = "Струйная продувка", RegenerationTime = 4.5, KoefK = 1.1},
            };
            FilterType = new List<FilterTypeClass>()
            {
                new FilterTypeClass{ Name="1250-1", FiltrSquare=1266,EntHands=252, EntSections=86,HandHeight=8, DiametrHand=200,Gabarit="5,10*6,84*13,77", Weight=37.8, idF = 0},
                new FilterTypeClass{ Name="1650-1", FiltrSquare=1688,EntHands=336, EntSections=8,HandHeight=8, DiametrHand=200,Gabarit="6,60*6,84*13,77", Weight=50.4, idF = 0 },
                new FilterTypeClass{ Name="2500-1", FiltrSquare=2530,EntHands=504, EntSections=12,HandHeight=8, DiametrHand=200,Gabarit="9,60*6,84*16,20", Weight=75.6, idF = 0 },
                new FilterTypeClass{ Name="4100-1", FiltrSquare=4104,EntHands=432, EntSections=8,HandHeight=10, DiametrHand=300,Gabarit="12,6*6,84*16,20", Weight=108.5, idF = 0 },
                new FilterTypeClass{ Name="5100-2", FiltrSquare=5130,EntHands=540, EntSections=10,HandHeight=10, DiametrHand=300,Gabarit="15,6*9,84*16,20", Weight=136, idF = 0 },
                new FilterTypeClass{ Name="6000-2", FiltrSquare=6156,EntHands=646, EntSections=12,HandHeight=10, DiametrHand=300,Gabarit="18,6*9,84*16,20", Weight=162.8, idF = 0 },
                new FilterTypeClass{ Name="7000-2", FiltrSquare=7182,EntHands=756, EntSections=14,HandHeight=10, DiametrHand=300,Gabarit="21,6*9,84*16,20", Weight=190, idF = 0 },
                new FilterTypeClass{ Name="8000-2", FiltrSquare=8208,EntHands=864, EntSections=16,HandHeight=10, DiametrHand=300,Gabarit="24,6*9,84*16,20", Weight=217, idF = 0 },
                new FilterTypeClass{ Name="20000-2", FiltrSquare=20520,EntHands=2160, EntSections=10,HandHeight=10, DiametrHand=300,Gabarit="30,4*21*22,64", Weight=540, idF = 0},
                new FilterTypeClass{ Name="24000-2", FiltrSquare=24624,EntHands=2592, EntSections=12,HandHeight=10, DiametrHand=300,Gabarit="36,4*21,0*22,64", Weight=650 , idF = 0},
                new FilterTypeClass{ Name="ФРКИ-30", FiltrSquare=30,EntHands=36, EntSections=1,HandHeight=2, DiametrHand=130,Gabarit="1,46*2,66*3,62", Weight=1.3, idF = 1 }, 
                new FilterTypeClass{ Name="ФРКИ-90", FiltrSquare=60,EntHands=72, EntSections=2,HandHeight=2, DiametrHand=130,Gabarit="2,82*2,06*3,62", Weight=2.1, idF = 1 },
                new FilterTypeClass{ Name="ФРКИ-90", FiltrSquare=90,EntHands=108, EntSections=3,HandHeight=2, DiametrHand=130,Gabarit="4,14*2,06*4,62", Weight=3, idF = 1 },
                new FilterTypeClass{ Name="ФРКИ-180", FiltrSquare=18,EntHands=144, EntSections=4,HandHeight=3, DiametrHand=130,Gabarit="5,48*2,06*4,62", Weight=4.6, idF = 1 },
                new FilterTypeClass{ Name="ФРКИ-360", FiltrSquare=360,EntHands=288, EntSections=8,HandHeight=3, DiametrHand=130,Gabarit="5,85*4,37*4,88", Weight=9.9, idF = 1 },
                new FilterTypeClass{ Name="ФРКДИ-500", FiltrSquare=550,EntHands=216, EntSections=6,Gabarit="1,94х4,37х9,21", Weight=20.5, DiametrHand=130, idF = 2 },
                new FilterTypeClass{ Name="ФРКДИ-500", FiltrSquare=720,EntHands=288, EntSections=8,Gabarit="6,28х4,37х9,21", Weight=28.7, DiametrHand=130, idF = 2 },
                new FilterTypeClass{ Name="ФРКДИ-500", FiltrSquare=1100,EntHands=432, EntSections=12,Gabarit="8,95х4,37х9,21", Weight=33, DiametrHand=130, idF = 2 },
                new FilterTypeClass{ Name="СМЦ-101А-1", FiltrSquare=50,EntHands=36, EntSections=4,Gabarit="3,2х1,7х9,2", Weight=2.4, DiametrHand=200, idF = 3 },
                new FilterTypeClass{ Name="СМЦ-101А-2", FiltrSquare=110,EntHands=36, EntSections=10,Gabarit="3,2х1,7х9,2", Weight=3.2, DiametrHand=200, idF = 3 },
                new FilterTypeClass{ Name="СМЦ-101А-3", FiltrSquare=287,EntHands=36, EntSections=10,Gabarit="3,2х1,7х13,9", Weight=14, DiametrHand=200, idF = 3 },
                new FilterTypeClass{ Name="УРФМ-11M", FiltrSquare=2300,EntHands=840,HandHeight=4.063, EntSections=20,Gabarit="23х4,8х13,4", Weight=14, DiametrHand=220, idF = 4 },
                new FilterTypeClass{ Name="УРФМ-11M", FiltrSquare=1610,EntHands=488,HandHeight=4.063, EntSections=14,Gabarit="16,1х4,8х13,1", Weight=14, DiametrHand=220, idF = 4 },
                new FilterTypeClass{ Name="ФРОС-9", FiltrSquare=9,EntHands=16,HandHeight=4.063, EntSections=4,Gabarit="4,5х2,17х1,8", Weight=1, DiametrHand=150, idF = 5 },
                new FilterTypeClass{ Name="ФРОС-13,5", FiltrSquare=13 ,EntHands=16,HandHeight=4.063, EntSections=4,Gabarit="5,5х2,17х1", Weight=1.1, DiametrHand=150, idF = 5 },
                new FilterTypeClass{ Name="ФРОС-20", FiltrSquare=20,EntHands=16,HandHeight=4.063, EntSections=6,Gabarit="5,2х2,76х1,6", Weight=2, DiametrHand=150, idF = 5 },
                new FilterTypeClass{ Name="ФРОС-31", FiltrSquare=31,EntHands=16,HandHeight=4.063, EntSections=6,Gabarit="6,2х2,76х1,6", Weight=2.3, DiametrHand=150, idF = 5 },
                new FilterTypeClass{ Name="ФРОС-66", FiltrSquare=66,EntHands=16,HandHeight=4.063, EntSections=8,Gabarit="6,9х3,36х2,2", Weight=3.7, DiametrHand=150, idF = 5 },
                new FilterTypeClass{ Name="РФСП-П", FiltrSquare=370,EntHands=88,HandHeight=4.063, EntSections=4,Gabarit="11,5х5,05х13,2", Weight=42, DiametrHand=360, idF = 6 },
                new FilterTypeClass{ Name="РФСП-1580", FiltrSquare=1580,EntHands=480,HandHeight=4.063, EntSections=20,Gabarit="25,75х7,74х11,77", Weight=180, DiametrHand=220, idF = 6 },
                new FilterTypeClass{ Name="ФРИ-630",  FiltrSquare=606,   Gabarit="4,52х4,22х9,325", Weight=15.03, DiametrHand=270,powerIsPower = 58.2,  idF = 7 },
                new FilterTypeClass{ Name="ФРИ-800",  FiltrSquare=808,   Gabarit="5,85х4,22х9,325", Weight=18.57, DiametrHand=270,powerIsPower = 77.6,  idF = 7 },
                new FilterTypeClass{ Name="ФРИ-1250", FiltrSquare=1212,Gabarit="8,54х4,22х9,325", Weight=25.52, DiametrHand=270,  powerIsPower = 116.5, idF = 7 },
                new FilterTypeClass{ Name="ФРИ-1600", FiltrSquare=1616,Gabarit="11,22х4,22х9,325", Weight=33.14, DiametrHand=270 ,powerIsPower = 155.2, idF = 7 },
                new FilterTypeClass{ Name="ФКИ-28", FiltrSquare=28,Gabarit="2,4х25,45х6,8", DiametrHand=150, powerIsPower = 3.36,  idF = 8 },
                new FilterTypeClass{ Name="ФКИ-84",  FiltrSquare=28,Gabarit="2,65х3,55х7,1", DiametrHand=150,powerIsPower = 11.09, idF = 8 },
                new FilterTypeClass{ Name="ФКИ-168",FiltrSquare=28,Gabarit="5,3х3,55х7,1", DiametrHand=150,  powerIsPower = 17.1,  idF = 8 },
            };
        }

        public class FilterTypeClass : FilterOptionsClass
        {
            public int idF { get; set; }// по айдишнику выврдить будем
        }
        public class FilterOptionsClass
        {
            public int FiltrSquare { get; set; }
            public int EntHands { get; set; }
            public int EntSections { get; set; }
            public int EntEMI { get; set; }
            public double HandHeight { get; set; }
            public int DiametrHand { get; set; }
            public string Gabarit { get; set; }
            public string Name { get; set; }
            public double Weight{ get; set; }
            public double powerIsPower { get; set; }

        }
        public class NaimenovaniaFiltrovClass
        {
            public string NameF { get; set; }
            public int id { get; set; }
        }
        public class MaterialTypeClass
        {
            public string Name { get; set; }
            public double H0 { get; set; }
            public double Poristost { get; set; }
        }
        public class DustTypeClass
        {
            public string Name { get; set; }
            public double Koeficient { get; set; }
        }
        public class TypeOfRegenerationClass
        {
            public string Name { get; set; }
        }
        public class filtrationSpeedClass
        {
            public string NameMaterial { get; set; }
            public string TypeOfReg { get; set; }
            public double RegenerationTime { get; set; }
            public double KoefK { get; set; }
        }
    }
}
