﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ClassLib; 

namespace UnitTestProject
{
    [TestClass]
    public class Tests
    {
        public double B3_sterlingConstGas = 124;
        public double B4_sterlingConstAir = 124;
        public double B5_sterlingConstWater = 961;
        public double B6_rashodDryClearedGasForNornalSituation = 125000;
        public double B7_temperatureComeOutGasForOchistka = 145;
        public double B8_airTemperature = 30;
        public double B9_allowedTemperatureWorkClearedGas = 130;
        public double B10_gasDensityNormalSituation = 1.3;
        public double B11_airDensityNormalSituation = 1.293;
        public double B12_gasViskosityNormalSituation = 17.9 * Math.Pow(10, -6);
        public double B13_airViskosityNormalSituation = 17.5 * Math.Pow(10, -6);
        public double B14_waterViskosityNormalSituation = 10 * Math.Pow(10, -6);
        public double B15_gasQUnderHisTemperature = 1.08;
        public double B16_gasQUnderHisAllowedTemperature = 1.07;
        public double B17_QAtmosphereAirUnderAllowedGasTemperature = 1.297;
        public double B18_waterSteamWhichContainsInOutComingGas = 0;
        public double B19_waterSteamWhichContainsInAtmosphereAir = 0;
        public double B20_BarometricPressureGround_Pbar = 101.3;
        public double B21_ExcessGasPressure_Rg = 0;
        public double B22_AverageDiameterDustParticles_dm = 3;
        public double B23_DustConcentrationInWasteGas_Zo = 13.3;
        public double B24_DustParticleDensity_ρh = 2800;
        //public string B25_TypeOfFilterMaterial_PR1 = "стеклоткань";
        //public string B26_FilterRegenerationMethod_PR2 = "обратная продувка";
        //public string B27_DustType_PR2 = "пыль кокса, золы, металла порошков, окислов металлов";
        public double B28_OptimalGasFilterSpeed_ωвх = 8;
        public double B29_ResistanceCoefficient_ξ = 2;
        public double B30_PermissibleResistanceDust = 0.3;
        public double B31_FiltrationRate_ωf = 0.57;
        public double B32_RecommendedFilterRegenerationTime = 60;
        public double B33_standardGasFilterLoad = 1.7;
        public double D16_K1 = 0.84;


        // h0 - стеклоткань (Таблица 8.2)
        public double h7 = 2700;
        // εтк - стеклоткань (Таблица 8.2)
        public double stekl = 0.52;


        //ВОЛШЕБНЫЕ ЧИСЛА

        public double G9 = 5130;
        public double G8 = 540;


        [TestMethod]
        public void Test_D2()
        {
            double rigth = 18586.966;
            double result = Math.Round(Properties.D2_airCountOnChill(B6_rashodDryClearedGasForNornalSituation, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature), 3);
            Assert.AreEqual(rigth, result, "  = {0}", rigth);
        }
        [TestMethod]
        public void Test_D3()
        {
            double rigth = 143586.966;
            double result = Math.Round((B6_rashodDryClearedGasForNornalSituation + Properties.D2_airCountOnChill(B6_rashodDryClearedGasForNornalSituation, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature)), 3);
            Assert.AreEqual(rigth, result, "  = {0}", rigth);
        }
        [TestMethod]
        public void Test_D4()
        {
            double rigth = 211961.7114;
            double a = 273;
            double result = Math.Round((Properties.D3_TotalAmountGas_Vog(B6_rashodDryClearedGasForNornalSituation, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature)) * (101.3 * (a + B9_allowedTemperatureWorkClearedGas)) / (a * (B20_BarometricPressureGround_Pbar - B21_ExcessGasPressure_Rg)), 4);
            Assert.AreEqual(rigth, result, "  = {0}", rigth);
        }
        [TestMethod]
        public void Test_D5()
        {
            double rigth = 211961.7114;
            double d2 = Properties.D2_airCountOnChill(B6_rashodDryClearedGasForNornalSituation, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature);
            double d3 = Properties.D3_TotalAmountGas_Vog(B6_rashodDryClearedGasForNornalSituation, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature);
            double d4 = Properties.D4_rashodUnderWorkingCase(B20_BarometricPressureGround_Pbar, B21_ExcessGasPressure_Rg, B6_rashodDryClearedGasForNornalSituation, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature);

            double result = Math.Round((d4 / d3) * (B6_rashodDryClearedGasForNornalSituation * (1 + B18_waterSteamWhichContainsInOutComingGas / 0.804) + d2 * (1 + B19_waterSteamWhichContainsInAtmosphereAir / 0.804)), 4);
            Assert.AreEqual(rigth, result, "  = {0}", rigth);
        }
        [TestMethod]
        public void Test_D6()
        {
            double rigth = 1.3;
            double result = Math.Round((0.804 * (B10_gasDensityNormalSituation + B18_waterSteamWhichContainsInOutComingGas) / (0.804 + B18_waterSteamWhichContainsInOutComingGas)), 1);
            Assert.AreEqual(rigth, result, "  = {0}", rigth);
        }

        [TestMethod]
        public void Test_D7()
        {
            double rigth = 1.293;
            double result = Math.Round(0.804 * (B11_airDensityNormalSituation + B19_waterSteamWhichContainsInAtmosphereAir) / (0.804 + B19_waterSteamWhichContainsInAtmosphereAir), 3);
            Assert.AreEqual(rigth, result, "  = {0}", rigth);
        }
        [TestMethod]
        public void Test_D8()
        {
            double rigth = 1.299;
            double result = Math.Round(Properties.D8_p_o_vl(B10_gasDensityNormalSituation, B18_waterSteamWhichContainsInOutComingGas, B6_rashodDryClearedGasForNornalSituation, B7_temperatureComeOutGasForOchistka, B11_airDensityNormalSituation, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B16_gasQUnderHisAllowedTemperature, B19_waterSteamWhichContainsInAtmosphereAir), 3);
            Assert.AreEqual(rigth, result, "  = {0}", rigth);
        }
        [TestMethod]
        public void Test_D9() //ready
        {
            double rigth = 0.8800;
            double d8 = Properties.D8_p_o_vl(B10_gasDensityNormalSituation, B18_waterSteamWhichContainsInOutComingGas, B6_rashodDryClearedGasForNornalSituation, B7_temperatureComeOutGasForOchistka, B11_airDensityNormalSituation, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B16_gasQUnderHisAllowedTemperature, B19_waterSteamWhichContainsInAtmosphereAir);
            double result = Math.Round((d8 * 273 * (B20_BarometricPressureGround_Pbar - B21_ExcessGasPressure_Rg)) / ((B9_allowedTemperatureWorkClearedGas + 273) * 101.3), 4);
            Assert.AreEqual(rigth, result, "  = {0}", rigth);
        }
        //2,4185E-05
        [TestMethod]
        public void Test_D10() //ready
        {
            double rigth = Math.Round(2.4185 * 0.00001, 7);
            double result = Math.Round(Properties.D10_u_gas(B12_gasViskosityNormalSituation, B3_sterlingConstGas, B9_allowedTemperatureWorkClearedGas), 7);
            Assert.AreEqual(rigth, result, "  = {0}", rigth);
        }
        [TestMethod]
        public void Test_D11()
        {
            double rigth = Math.Round(2.3644 * Math.Pow(10, -5), 7); //2,36446E-05

            double result = Math.Round(Properties.D11_u_vosduh(B13_airViskosityNormalSituation, B4_sterlingConstAir, B9_allowedTemperatureWorkClearedGas), 7);
            Assert.AreEqual(rigth, result, "  = {0}", rigth);
        }
        [TestMethod]
        public void Test_D12()
        {
            double rigth = Math.Round(1.6226 * Math.Pow(10, -5), 7);  //1,62261E-05
            double result = Math.Round(Properties.D12_u_voda(B14_waterViskosityNormalSituation, B5_sterlingConstWater, B9_allowedTemperatureWorkClearedGas), 7);
            Assert.AreEqual(rigth, result, "  = {0}", rigth);
        }
        [TestMethod]
        public void Test_D13()
        {
            double rigth = Math.Round(4.0341 * Math.Pow(10, -5), 5);
            double d2 = Properties.D2_airCountOnChill(B6_rashodDryClearedGasForNornalSituation, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature);
            double d3 = Properties.D3_TotalAmountGas_Vog(B6_rashodDryClearedGasForNornalSituation, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature);
            double d4 = Properties.D4_rashodUnderWorkingCase(B20_BarometricPressureGround_Pbar, B21_ExcessGasPressure_Rg, B6_rashodDryClearedGasForNornalSituation, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature);
            double d5 = Properties.D5_Vg(B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg);
            double d10 = Properties.D10_u_gas(B12_gasViskosityNormalSituation, B3_sterlingConstGas, B9_allowedTemperatureWorkClearedGas);
            double d11 = Properties.D11_u_vosduh(B13_airViskosityNormalSituation, B4_sterlingConstAir, B9_allowedTemperatureWorkClearedGas);
            double d12 = Properties.D12_u_voda(B14_waterViskosityNormalSituation, B5_sterlingConstWater, B9_allowedTemperatureWorkClearedGas);


            double result = Math.Round((d10 * B6_rashodDryClearedGasForNornalSituation / d3 + d11 * d2 / d3 + d12 * d4 / d5), 5);
            Assert.AreEqual(rigth, result, "  = {0}", rigth);
        }
        [TestMethod]
        public void Test_D14()
        {
            double rigth = Math.Round(7.843397703, 5);
            double result = Math.Round(Properties.D14_Z(B23_DustConcentrationInWasteGas_Zo, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg), 5);
            Assert.AreEqual(rigth, result, "  = {0}", rigth);
        }
        [TestMethod]
        public void Test_D15()
        {
            double rigth = Math.Round(0.833595656, 3);
            double result = Math.Round(Properties.D15_qf(B22_AverageDiameterDustParticles_dm, B33_standardGasFilterLoad, D16_K1, B23_DustConcentrationInWasteGas_Zo, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg), 3);
            Assert.AreEqual(rigth, result, "  = {0}", rigth);
        }




        [TestMethod]
        public void Test_D20_2()
        {
            double rigth = 0.7996;
            double result = Math.Round(Properties.D20_2(B22_AverageDiameterDustParticles_dm), 4);
            Assert.AreEqual(rigth, result, "  = {0}", rigth);
        }
        [TestMethod]
        public void Test_D21()
        {
            double rigth = 663453539.6;
            double result = Properties.D21_MoreThenEnoghtDavlenie_A_K5(B22_AverageDiameterDustParticles_dm, h7, stekl); // стеклоткань - 0,52

            Assert.AreEqual(rigth, Math.Round(result, 1), "  = {0}", rigth);
        }
        [TestMethod]
        public void Test_D22()
        {
            double rigth = 0.25426;
            double result = Properties.D22_Δp1(h7, stekl, B22_AverageDiameterDustParticles_dm, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg, B12_gasViskosityNormalSituation, B3_sterlingConstGas, B13_airViskosityNormalSituation, B4_sterlingConstAir, B14_waterViskosityNormalSituation, B5_sterlingConstWater, B31_FiltrationRate_ωf);
            Assert.AreEqual(rigth, Math.Round(result, 5), "  = {0}", rigth);
        }

        [TestMethod]
        public void Test_D25()
        {
            double rigth = 12707481015;
            double result = Properties.D25_B(B22_AverageDiameterDustParticles_dm, B24_DustParticleDensity_ρh);
            Assert.AreEqual(rigth, Math.Round(result, 5), "  = {0}", rigth);

        }



        [TestMethod]
        public void Test_D26_timeFull()
        {
            double right = 2203.5315;

            double result = Properties.D26(h7, stekl, B24_DustParticleDensity_ρh, B23_DustConcentrationInWasteGas_Zo, B22_AverageDiameterDustParticles_dm, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg, B12_gasViskosityNormalSituation, B3_sterlingConstGas, B13_airViskosityNormalSituation, B4_sterlingConstAir, B14_waterViskosityNormalSituation, B5_sterlingConstWater, B31_FiltrationRate_ωf);
            Assert.AreEqual(Math.Round(right, 1), Math.Round(result, 1), "  = {0}", right);
        }
        [TestMethod]
        public void Test_D27_rez()
        {
            double right = 1.5904;
            double result = Properties.D27(h7, stekl, B32_RecommendedFilterRegenerationTime, B24_DustParticleDensity_ρh, B23_DustConcentrationInWasteGas_Zo, B22_AverageDiameterDustParticles_dm, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg, B12_gasViskosityNormalSituation, B3_sterlingConstGas, B13_airViskosityNormalSituation, B4_sterlingConstAir, B14_waterViskosityNormalSituation, B5_sterlingConstWater, B31_FiltrationRate_ωf);
            Assert.AreEqual(right, Math.Round(result, 4), "  = {0}", right);
        }
        [TestMethod]
        public void Test_D28_QqantityAAir()
        {
            double right = 5618.522;
            double result = Properties.D28_Vp(h7, stekl, B32_RecommendedFilterRegenerationTime, B24_DustParticleDensity_ρh, B23_DustConcentrationInWasteGas_Zo, B22_AverageDiameterDustParticles_dm, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg, B12_gasViskosityNormalSituation, B3_sterlingConstGas, B13_airViskosityNormalSituation, B4_sterlingConstAir, B14_waterViskosityNormalSituation, B5_sterlingConstWater, B31_FiltrationRate_ωf);
            Assert.AreEqual(Math.Round(right, 1), Math.Round(result, 1), "  = {0}", right);
        }

        [TestMethod]
        public void Test_D29_Ff()
        {
            double right = 4350.235279;
            double result = Properties.D29_Ff(h7, stekl, D16_K1, B33_standardGasFilterLoad, B32_RecommendedFilterRegenerationTime, B24_DustParticleDensity_ρh, B23_DustConcentrationInWasteGas_Zo, B22_AverageDiameterDustParticles_dm, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg, B12_gasViskosityNormalSituation, B3_sterlingConstGas, B13_airViskosityNormalSituation, B4_sterlingConstAir, B14_waterViskosityNormalSituation, B5_sterlingConstWater, B31_FiltrationRate_ωf);
            Assert.AreEqual(Math.Round(right, 2), Math.Round(result, 2), "  = {0}", right);
        }

        [TestMethod]
        public void Test_D30()
        {
            double right = 9.5;
            double result = Properties.D30(G9, G8);
            Assert.AreEqual(Math.Round(right, 2), Math.Round(result, 2), "  = {0}", right);
        }

        [TestMethod]
        public void Test_D31()
        {
            double right = 135.982202;
            double result = Properties.D31(h7, stekl, G9, B32_RecommendedFilterRegenerationTime, B24_DustParticleDensity_ρh, B23_DustConcentrationInWasteGas_Zo, B22_AverageDiameterDustParticles_dm, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg, B12_gasViskosityNormalSituation, B3_sterlingConstGas, B13_airViskosityNormalSituation, B4_sterlingConstAir, B14_waterViskosityNormalSituation, B5_sterlingConstWater, B31_FiltrationRate_ωf);
            Assert.AreEqual(Math.Round(right, 2), Math.Round(result, 2), "  = {0}", right);
        }

        [TestMethod]
        public void Test_D32()
        {
            double right = 4650.591308;
            double result = Properties.D32(h7, stekl, G9, B32_RecommendedFilterRegenerationTime, B24_DustParticleDensity_ρh, B23_DustConcentrationInWasteGas_Zo, B22_AverageDiameterDustParticles_dm, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg, B12_gasViskosityNormalSituation, B3_sterlingConstGas, B13_airViskosityNormalSituation, B4_sterlingConstAir, B14_waterViskosityNormalSituation, B5_sterlingConstWater, B31_FiltrationRate_ωf);
            Assert.AreEqual(Math.Round(right, 1), Math.Round(result, 1), "  = {0}", right);
        }

        [TestMethod]
        public void Test_D33()
        {
            double right = 4466.864949;
            double result = Properties.D33(h7, stekl, D16_K1, B33_standardGasFilterLoad, G9, B32_RecommendedFilterRegenerationTime, B24_DustParticleDensity_ρh, B23_DustConcentrationInWasteGas_Zo, B22_AverageDiameterDustParticles_dm, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg, B12_gasViskosityNormalSituation, B3_sterlingConstGas, B13_airViskosityNormalSituation, B4_sterlingConstAir, B14_waterViskosityNormalSituation, B5_sterlingConstWater, B31_FiltrationRate_ωf);
            Assert.AreEqual(Math.Round(right, 1), Math.Round(result, 1), "  = {0}", right);
        }

        [TestMethod]
        public void Test_D34()
        {
            double right = 12.92660917;
            double result = Properties.D34(h7, stekl, D16_K1, B33_standardGasFilterLoad, G9, B32_RecommendedFilterRegenerationTime, B24_DustParticleDensity_ρh, B23_DustConcentrationInWasteGas_Zo, B22_AverageDiameterDustParticles_dm, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg, B12_gasViskosityNormalSituation, B3_sterlingConstGas, B13_airViskosityNormalSituation, B4_sterlingConstAir, B14_waterViskosityNormalSituation, B5_sterlingConstWater, B31_FiltrationRate_ωf);
            Assert.AreEqual(Math.Round(right, 1), Math.Round(result, 1), "  = {0}", right);
        }

        [TestMethod]
        public void Test_D35()
        {
            double right = 32340;
            double result = Properties.D35(G8, B32_RecommendedFilterRegenerationTime);
            Assert.AreEqual(Math.Round(right, 1), Math.Round(result, 1), "  = {0}", right);
        }

        [TestMethod]
        public void Test_D36()
        {
            double right = 0.691458715;
            double result = Properties.D36(G9, G8, B22_AverageDiameterDustParticles_dm, B33_standardGasFilterLoad, D16_K1, B23_DustConcentrationInWasteGas_Zo, B6_rashodDryClearedGasForNornalSituation, B18_waterSteamWhichContainsInOutComingGas, B19_waterSteamWhichContainsInAtmosphereAir, B20_BarometricPressureGround_Pbar, B7_temperatureComeOutGasForOchistka, B16_gasQUnderHisAllowedTemperature, B9_allowedTemperatureWorkClearedGas, B15_gasQUnderHisTemperature, B17_QAtmosphereAirUnderAllowedGasTemperature, B8_airTemperature, B21_ExcessGasPressure_Rg);
            Assert.AreEqual(Math.Round(right, 1), Math.Round(result, 1), "  = {0}", right);
        }

        [TestMethod]
        public void Test_D37()
        {
            double right = -20.55609949;
            double result = Properties.D37( G9, G8,  B22_AverageDiameterDustParticles_dm,  B33_standardGasFilterLoad,  D16_K1,  B23_DustConcentrationInWasteGas_Zo,  B6_rashodDryClearedGasForNornalSituation,  B18_waterSteamWhichContainsInOutComingGas,  B19_waterSteamWhichContainsInAtmosphereAir,  B20_BarometricPressureGround_Pbar,  B7_temperatureComeOutGasForOchistka,  B16_gasQUnderHisAllowedTemperature,  B9_allowedTemperatureWorkClearedGas,  B15_gasQUnderHisTemperature,  B17_QAtmosphereAirUnderAllowedGasTemperature,  B8_airTemperature,  B21_ExcessGasPressure_Rg);
            Assert.AreEqual(Math.Round(right, 1), Math.Round(result, 1), "  = {0}", right);
        }

        [TestMethod]
        public void Test_D38()
        {
            double right = 164.1901534;
            double result = Properties.D38( h7,  stekl,  B24_DustParticleDensity_ρh,  B23_DustConcentrationInWasteGas_Zo,  B22_AverageDiameterDustParticles_dm,  B6_rashodDryClearedGasForNornalSituation,  B18_waterSteamWhichContainsInOutComingGas,  B19_waterSteamWhichContainsInAtmosphereAir,  B20_BarometricPressureGround_Pbar,  B7_temperatureComeOutGasForOchistka,  B16_gasQUnderHisAllowedTemperature,  B9_allowedTemperatureWorkClearedGas,  B15_gasQUnderHisTemperature,  B17_QAtmosphereAirUnderAllowedGasTemperature,  B8_airTemperature,  B21_ExcessGasPressure_Rg,  B12_gasViskosityNormalSituation,  B3_sterlingConstGas,  B13_airViskosityNormalSituation,  B4_sterlingConstAir,  B14_waterViskosityNormalSituation,  B5_sterlingConstWater,  B31_FiltrationRate_ωf);
            Assert.AreEqual(Math.Round(right, 1), Math.Round(result, 1), "  = {0}", right);
        }

        [TestMethod]
        public void Test_D39()
        {
            double right = 1.559806457;
            double result = Properties.D39( h7,  stekl, G8,  G9,  B24_DustParticleDensity_ρh,  B23_DustConcentrationInWasteGas_Zo,  B22_AverageDiameterDustParticles_dm,  B6_rashodDryClearedGasForNornalSituation,  B18_waterSteamWhichContainsInOutComingGas,  B19_waterSteamWhichContainsInAtmosphereAir,  B20_BarometricPressureGround_Pbar,  B7_temperatureComeOutGasForOchistka,  B16_gasQUnderHisAllowedTemperature,  B9_allowedTemperatureWorkClearedGas,  B15_gasQUnderHisTemperature,  B17_QAtmosphereAirUnderAllowedGasTemperature,  B8_airTemperature,  B21_ExcessGasPressure_Rg,  B12_gasViskosityNormalSituation,  B3_sterlingConstGas,  B13_airViskosityNormalSituation,  B4_sterlingConstAir,  B14_waterViskosityNormalSituation,  B5_sterlingConstWater,  B31_FiltrationRate_ωf);
            Assert.AreEqual(Math.Round(right, 1), Math.Round(result, 1), "  = {0}", right);
        }




        [TestMethod]
        public void Test_D40()
        {
            double rigth = 0.7996;
            double result = Math.Round(Properties.D40_poristostSloiaDustEp(B22_AverageDiameterDustParticles_dm), 4);
            Assert.AreEqual(rigth, result, "  = {0}", rigth);
        }
    }
}

